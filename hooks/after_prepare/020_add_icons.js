#!/usr/bin/env node
 
// Icones gerados pelo site: http://pgicons.abiro.com/
// Para atualizar icones, gerar .zip na url acima e extrair no diretório config/
 
var filestocopy = [{
    "config/res/icons/android/icon-48-mdpi.png": 
    "platforms/android/res/drawable/icon.png"
}, {
    "config/res/icons/android/icon-72-hdpi.png": 
    "platforms/android/res/drawable-hdpi/icon.png"
}, {
    "config/res/icons/android/icon-36-ldpi.png": 
    "platforms/android/res/drawable-ldpi/icon.png"
}, {
    "config/res/icons/android/icon-48-mdpi.png": 
    "platforms/android/res/drawable-mdpi/icon.png"
}, {
    "config/res/icons/android/icon-96-xhdpi.png": 
    "platforms/android/res/drawable-xhdpi/icon.png"
// }
// , {
//     "config/res/icons/android/icon-48-mdpi.png": 
//     "platforms/android/res/drawable/splash.png"
// }, {
//     "config/res/icons/android/icon-48-mdpi.png": 
//     "platforms/android/res/drawable-hdpi/splash.png"
// }, {
//     "config/res/icons/android/icon-48-mdpi.png": 
//     "platforms/android/res/drawable-ldpi/splash.png"
// }, {
//     "config/res/icons/android/icon-48-mdpi.png": 
//     "platforms/android/res/drawable-mdpi/splash.png"
// }, {
//     "config/res/icons/android/icon-48-mdpi.png": 
//     "platforms/android/res/drawable-xhdpi/splash.png"
// }, {
//     "config/ios/Resources/icons/icon-72.png": 
//     "platforms/ios/YourAppName/Resources/icons/icon-72.png"
// }, {
//     "config/ios/Resources/icons/icon.png": 
//     "platforms/ios/YourAppName/Resources/icons/icon.png"
// }, {
//     "config/ios/Resources/icons/icon@2x.png": 
//     "platforms/ios/YourAppName/Resources/icons/icon@2x.png"
// }, {
//     "config/ios/Resources/icons/icon-72@2x.png": 
//     "platforms/ios/YourAppName/Resources/icons/icon-72@2x.png"
// }, {
//     "config/ios/Resources/splash/Default@2x~iphone.png": 
//     "platforms/ios/YourAppName/Resources/splash/Default@2x~iphone.png"
// }, {
//     "config/ios/Resources/splash/Default-568h@2x~iphone.png": 
//     "platforms/ios/YourAppName/Resources/splash/Default-568h@2x~iphone.png"
// }, {
//     "config/ios/Resources/splash/Default~iphone.png":
//     "platforms/ios/YourAppName/Resources/splash/Default~iphone.png"
// }, {
//     "config/ios/Resources/splash/Default-Portrait~ipad.png": 
//      "platforms/ios/YourAppName/Resources/splash/Default-Portrait~ipad.png"
// }, {
//     "config/ios/Resources/splash/Default-Portrait@2x~ipad.png": 
//     "platforms/ios/YourAppName/Resources/splash/Default-Portrait@2x~ipad.png"
}, ];
 
var fs = require('fs');
var path = require('path');
 
// no need to configure below
var rootdir = process.argv[2];
 
filestocopy.forEach(function(obj) {
    Object.keys(obj).forEach(function(key) {
        var val = obj[key];
        var srcfile = path.join(rootdir, key);
        var destfile = path.join(rootdir, val);
        //console.log("copying "+srcfile+" to "+destfile);
        var destdir = path.dirname(destfile);
        if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
            fs.createReadStream(srcfile).pipe(
               fs.createWriteStream(destfile));
        }
    });
});