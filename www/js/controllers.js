var db = null;

angular.module('starter.controllers', ['ionic', 'ngCordova', 'ngCookies', 'ion-floating-menu'])
.run(function($rootScope, $ionicPlatform, $cordovaSQLite) {

  $ionicPlatform.ready(function() {

    if(window.cordova) {
      db = $cordovaSQLite.openDB({name:"opera-expositor.db", location:'default'});
    } else {
          db = window.openDatabase("opera-expositor.db", '1', 'auto', 1024 * 1024 * 100); // browser
        }

        // $cordovaSQLite.execute(db, 'DROP TABLE Credential');
        // $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Credential (id INTEGER PRIMARY KEY AUTOINCREMENT, visitoredition_id BIG INTEGER, day_visited DATE, date_added DATETIME, oncloud BOOLEAN, name TEXT, document TEXT, profile TEXT)');

        // $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS Input');
        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Input (id INTEGER PRIMARY KEY AUTOINCREMENT, credenciamento_id BIG INTEGER NOT NULL, date_added DATETIME, oncloud BOOLEAN NOT NULL)');
        $cordovaSQLite.execute(db, 'CREATE UNIQUE INDEX credenciamento_idx on Input (credenciamento_id)');


      });
})
.config(['$httpProvider', function($httpProvider, $ionicConfigProvider) {
      $httpProvider.defaults.withCredentials = true; // NAO REMOVER! DJANGO DEPENDE DISSO PARA MANTER USER AUTENTICADO

    }])

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.navBar.alignTitle('center');
})

/**********************************
 * LEITOR QRCODE E LISTAGEM DE LEITURAS
 **********************************/

 .controller('DashCtrl', function($scope, $state, $rootScope, $cordovaNetwork, $ionicPlatform, $cordovaSQLite, $ionicPopup, SyncService, LoginService, VisitorService) {
  if(!window.cordova) {
      $scope.devmode = true; // offline. show login form by default
    }

      $scope.is_authenticated = false; // not authenticated by default
      $scope.is_online = false; // not authenticated by default

      $scope.list = function() {
        VisitorService.list().then(function(response) {
          if(response.data.credenciamentos.length>0) {
            $scope.edition_visitors = response.data.credenciamentos;
          } else {
            $scope.noresults_available_cloud = true;
          }
        });
      }

      /** warning de leituras nao sincronizadas no cloud */
      $scope.checkunsynced = function() {
        $cordovaSQLite.execute(db, "SELECT count(*) as total FROM Input WHERE oncloud='false'").then(function(result) {
          if(result.rows.item(0).total > 0) {
            $scope.unsynceditenswarning = true;
            $scope.unsynceditenstotal = result.rows.item(0).total;
          } else {
            $scope.unsynceditenswarning = false;
          }
        });
      }
      /** warning de leituras nao sincronizadas no cloud */
      $scope.haveatleastoneinput = function() {
        $cordovaSQLite.execute(db, "SELECT count(*) as total FROM Input").then(function(result) {
          if(result.rows.item(0).total == 0) {
            $scope.noresults_available_local = true;
          } 
        });
      }

      $scope.scan = function(){
       $ionicPlatform.ready(function() {
      
cordova.plugins.barcodeScanner.scan(

    // success callback function
    function (result) {
        // wrapping in a timeout so the dialog doesn't free the app
        setTimeout(function() {
            $scope.save(result.text);
            // alert("We got a barcode\n" +
            //       "Result: " + result.text + "\n" +
            //       "Format: " + result.format + "\n" +
            //       "Cancelled: " + result.cancelled);                            
        }, 0);
    },

    // error callback function
    function (error) {
        alert("Erro ao realizar leitura: " + error);
    },

    // options object
    {
        "preferFrontCamera" : false,
        "showFlipCameraButton" : true,
        "showTorchButton" : true,
        "orientation" : "portrait"
    }
    )

      })
     }

     $scope.synctocloud = function() {
      if($scope.is_authenticated && $scope.is_online) {
        $cordovaSQLite.execute(db, "SELECT * FROM Input WHERE oncloud='false'")
        .then(function(result) {
          if(result.rows.length > 0) {
            var leituras = [];
            for (var i=0; i<result.rows.length; i++) {
              leituras.push({
                credenciamento_id:   result.rows.item(i).credenciamento_id,
                date_added: result.rows.item(i).date_added
              })
            }
            SyncService.synctocloud(leituras).success(function(result){
              if(result.invalids.length > 0) { // leituras invalidas
                var alertPopup = $ionicPopup.alert({
                  title: 'Aviso',
                  template: result.invalids.length + ' registro(s) não são válidos e serão removidos. Clique OK para continuar.'
                }).then(function() {
                  for(i=0; i<result.invalids.length; i++) {
                    $cordovaSQLite.execute(db, "DELETE FROM Input WHERE credenciamento_id=?",[result.invalids[i]]);
                    $scope.checkunsynced();                    
                  }
                }); 
              }
              if(result.valids.length > 0) { // leituras validas
                  for(i=0; i<result.valids.length; i++) { // leitura registrada no servidor -> informa banco local
                    $cordovaSQLite.execute(db, "UPDATE Input SET oncloud='true' where credenciamento_id=?",[result.valids[i].credenciamento_id]);
                  }
                  
                } else {
                  $scope.noresults_available_cloud = true;
                }
              $scope.checkunsynced(); // remove msg if exists
              $scope.list(); // reload list
            });
          }}, function(error) {
            console.log("SQL: Erro ao registrar ");
          })
      }
    };

    $scope.save = function(credenciamento_id) {
      $cordovaSQLite.execute(db, "SELECT count(*) as total FROM Input WHERE credenciamento_id = ?", [credenciamento_id,])
      .then(function(result) {
            if(result.rows.item(0).total == 0 && parseInt(credenciamento_id) > 0) { // insere registro de visitante no dia, caso visitante nao tenha entrado ainda
              $cordovaSQLite.execute(db, 'INSERT INTO Input (credenciamento_id, date_added, oncloud) VALUES (?, datetime(\'now\', \'localtime\'),?)', [credenciamento_id, 'false'])
              .then(function(result) {
                // if(!$scope.is_authenticated || !$scope.is_online){
                  $scope.checkunsynced();
                // } else {
                  $scope.synctocloud();
                  $scope.noresults_available_local = false;
                // }
              }, function(error) {
                $scope.statusMessage = "SQL: Erro ao salvar registro: " + error.message;
              })
            } else {
              if(parseInt(credenciamento_id) > 0) { // nao dispara erro caso usuario cancele a leitura
                var alertPopup = $ionicPopup.alert({
                  title: 'Cadastro já existente',
                  template: 'O Expositor já foi registrado em sua base de dados local. Utilize a função Exportar para coletar dados completos.'
                });
              }
            }
          }, function(error) {
            $scope.statusMessage = "SQL: Erro ao registrar expositor: " + error.message;
          })
    };


    $scope.insert_test = function() {
      $scope.save($scope.data.visitorinputtest);

    };

    $scope.finishwizard = function() {
      window.localStorage.setItem("wizardisfinished", true);
      $scope.wizard_class = 'hide';
      $scope.wizardisfinished = 'true';
    };

    $ionicPlatform.ready(function() {
      if(db) {
        $scope.checkunsynced();
        $scope.haveatleastoneinput();
        // $scope.firstlogin = window.localStorage.getItem("firstlogin");

        // wizard restart
        // window.localStorage.setItem("wizardisfinished", false);

        $scope.wizardisfinished = window.localStorage.getItem("wizardisfinished");


        /** test form and list */
        if(!window.cordova) {
        $scope.devmode = true; // add input text to test keyboard inputs
        $cordovaSQLite.execute(db, "SELECT * FROM Input")
        .then(function(result) {
          var dbdump = [];
          for (var i=0; i<result.rows.length; i++) {
              // console.log(result.rows.length);
              dbdump.push({
                credenciamento_id:   result.rows.item(i).credenciamento_id,
                expositor_id:   result.rows.item(i).expositor_id,
                date_added: result.rows.item(i).date_added,
                oncloud: result.rows.item(i).oncloud,
              })
            }
            $scope.dbdump = dbdump;
          });
      }

      if(navigator.onLine || (window.cordova && $cordovaNetwork.isOnline())) {
        $scope.is_online = true;
        LoginService.login_check().success(function(data) {
          $scope.is_authenticated = data.is_authenticated == 'true' ? true : false;
          if(data.is_authenticated) {
            $scope.synctocloud();
            $scope.list();
          }
        });
      } else {
        $scope.no_connection = true;
      }
    }
  });

  })


/***********************************
 * Detalhes do Visitante
 **********************************/
 .controller('VisitorCtrl', function($scope, $state, $stateParams, $ionicPlatform, VisitorService) {
  $ionicPlatform.ready(function() {
    VisitorService.detail($stateParams.pk).then(function(response) {
      $scope.list = response.data.credenciamentos;
    });

    if(navigator.onLine || (window.cordova && $cordovaNetwork.isOnline())) {
      $scope.is_online = true;
    } else {
      $scope.no_connection = true;
    }
  });

  $scope.openurlsystem = function(url) {
    window.open(url,'_system');        
  }
})


/***********************************
 * RELATORIOS/VISITACOES
 **********************************/

 .controller('ReportCtrl', function($scope, $state, $rootScope, $cordovaNetwork, $ionicPlatform, LoginService, ReportService) {
  $scope.is_authenticated = 'false';
  if(navigator.onLine || (window.cordova && $cordovaNetwork.isOnline())) {
    LoginService.login_check().success(function(data) {
      $scope.is_authenticated = data.is_authenticated;
      ReportService.editions().success(function(result) { // usado apenas para o filtro de edicoes
            // console.log(result);
            $scope.editionsfilter = result.editions;
          });
      ReportService.visittable().success(function(result) { // tabela com dados de visitacoes
            // console.log(result);
            $scope.editions = result;
          });
    });
  } else {
      $scope.no_connection = true;
  }


  $scope.export = function(edition_id, profile) {
    ReportService.getExportLink(edition_id, profile).success(function(data) {
            // window.open(data,'_system'); // download for android only
            // return false;
            // exporta (share) link unico com planilha XLS
            window.plugins.socialsharing.share('Dados coletados na Feira Ópera', 'Expositores Feira Ópera', null, data)
          })
    }

  $scope.editionswitch = function(edition_pk) {
      ReportService.visittable_edition(edition_pk).success(function(result) { // tabela com dados de visitacoes
            $scope.editions = result;
          });
    }
})


/*****************************
 * LOGIN
 ***************************/
 .controller('SyncCtrl', function($scope, $cordovaSQLite, $ionicPopup, $ionicPlatform, $cordovaNetwork, LoginService, SyncService ) {
  $scope.data = {};
  $ionicPlatform.ready(function() {
      if(navigator.onLine || (window.cordova && $cordovaNetwork.isOnline())) {
        LoginService.login_check().success(function(data) {
          $scope.is_authenticated = data.is_authenticated;
          $scope.loggedEmail = window.localStorage.getItem("email");
        });
      } else {
          $scope.is_authenticated = 'false'; // offline. show login form by default
          $scope.no_connection = true;
      }
    });

      $scope.logout = function() {
        LoginService.logout().success(function(data) {
          $scope.is_authenticated = data.is_authenticated;
        })
      }


      $scope.login = function() {
        if($scope.data.hasOwnProperty('email') && $scope.data.hasOwnProperty('password')) {
          LoginService.loginUser($scope.data.email, $scope.data.password).success(function(data) {

            if(data.is_authenticated == 'true') {
              $scope.is_authenticated = 'true';
              $scope.userEmail = data.email; 
              window.localStorage.setItem("email", data.email);
            } else { 
              var alertPopup = $ionicPopup.alert({
                title: 'Login incorreto',
                template: 'Dados informados inválidos. Utilize o botão recuperar senha ou crie uma nova conta.'
              });
            }

          })
        };
      };


      $scope.openurlsystem = function(url) {
        window.open(url,'_system');        
      }

      $ionicPlatform.ready(function() {
        $scope.loggedEmail = window.localStorage.getItem("email");
      });

    });


