/*
* @Author: czd
* @Date:   2017-05-08 12:17:32
* @Last Modified by:   czd
* @Last Modified time: 2017-05-19 21:58:15
*/

var devel = false;

switch(devel) {
  case true:
    var api_url_base = 'http://192.168.0.10:8000/api'; // API BASE. Ex: http://192.168.0.10:8000/api
    break;
  default:
    var api_url_base = 'http://sigo.grupoopera.com.br/api'; // API BASE. Ex: http://192.168.0.10:8000/api
}

/** global get method */
function http_get(http, rootscope, ionicloading, url) {
  ionicloading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0 })
  return http.get(url).error(function(error) {
    rootscope.no_connection = true;
    ionicloading.hide();
  }).success(function(success){
    rootscope.no_connection = false;
    ionicloading.hide();
  });
  ionicloading.hide(); // to have sure!
}

/** global post method */
function http_post(http, rootscope, ionicloading, url, params) {
  ionicloading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0 })
  return http.post(url, params).error(function(error) {
    rootscope.no_connection = true;
    ionicloading.hide();
  }).success(function(success){
    rootscope.no_connection = false;
    ionicloading.hide();
  });
  ionicloading.hide(); // to have sure!
}

angular.module('starter.services', ['ionic','ngCordova','ngCookies'])

// .run( function run( $http, $cookies ){

//     $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
//     $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

// })
.service('LoginService', function($q, $http, $rootScope, $ionicLoading) {
    return {
        loginUser: function(email, pw) {
            var url = api_url_base + '/expositor/login/';
            return http_post($http, $rootScope, $ionicLoading, url, {'email' : email, 'password': pw});

        },
        login_check: function() {
             var url = api_url_base + '/login_check/';
             return http_post($http, $rootScope, $ionicLoading, url, {});
        },
        logout: function(email, pw) {
             var url = api_url_base + '/logout/';
             return http_post($http, $rootScope, $ionicLoading, url, {});
        }
    }
})
.service('ReportService', function($q, $http, $rootScope, $ionicLoading) {
    return {
        editions: function() {
            var url = api_url_base + '/expositor/editions/';
            return http_get($http, $rootScope, $ionicLoading, url, {});
        },
        visittable: function() {
            var url = api_url_base + '/expositor/report/table/';
            return http_get($http, $rootScope, $ionicLoading, url, {});
        },
        visittable_edition: function(edition_pk) {
            var url = api_url_base + '/expositor/report/table/' + edition_pk + '/';
            return http_get($http, $rootScope, $ionicLoading, url, {});
        },
        getExportLink: function(edition_id, profile) {
            var url = api_url_base + '/expositor/get_export_uniqueurl/' + edition_id + '/' + profile + '/';
            return http_get($http, $rootScope, $ionicLoading, url, {});

        },
    }
})
.service('VisitorService', function($q, $http, $rootScope, $ionicLoading) {
    return {
        list: function() {
            var url = api_url_base + '/expositor/visitor/list/';
            return http_get($http, $rootScope, $ionicLoading, url, {});
        },
        detail: function(pk) {
          var url = api_url_base + '/expositor/visitor/detail/'+pk+'/';
          return http_get($http, $rootScope, $ionicLoading, url);
        }

    }
})
.service('SyncService', function($q, $http, $rootScope, $ionicLoading) {
    return {
        // syncItem: function(visitoredition_id, date_added, day_visited) {
        //     var url = api_url_base + '/expositor/sync/';
        //     var params = {'visitoredition_id' : visitoredition_id, 'date_added': date_added, 'day_visited':day_visited}
        //     return http_post($http, $rootScope, $ionicLoading, url, params);
        // },
        synctocloud: function(leituras) {
            var url = api_url_base + '/expositor/sync/';
            return http_post($http, $rootScope, $ionicLoading, url, leituras);
        }
    }
});

